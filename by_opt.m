function [o_opt] = by_opt(data)

    % Get unary weights
    % 
    data = get_unary(data);

    % Get pairwise weights
    data = get_pairwise(data);

    % Construct the LP
    data = get_formulation(data);

    % Solve LP
    data = solve_lp(data);

    % Get the output by Opt
    o_opt = select_best(data);    

end

function o_opt = select_best(data)

    unary_length = length(data.unary);

    cdir = reshape(data.x(1:unary_length), 20, data.n_cand);

    o_opt = uint16(zeros(20, 2));
    for i=1:20
        for j=1:data.n_cand
            o_opt(i,:) = o_opt(i,:) + data.cand_fids{j}(i,:) * cdir(i, j);
        end
    end 

end
