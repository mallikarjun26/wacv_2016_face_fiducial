function [data] = validate(img, input_cand_fids)

    % All the constants
    min_s = 250;
    resize_s = [300 300];

    %
    img_s = size(img);
    
    % Throw error if the image size is too small
    if( img_s(1) < min_s || img_s(2) < min_s )
        error('Image resolution should be greater than 250x250')
    end
    
    % Throw error if the number of fiducials are not equal to 20
    n_cand = length(input_cand_fids);
    for i=1:n_cand
        if(size(input_cand_fids{i}, 1) ~= 20)
            error('Number of fiducials provided isnt equal to 20')
        end
    end    


    % Resize all the fids 
    cand_fids = cell(n_cand, 1);
    for i=1:n_cand
        t_fid = input_cand_fids{i};
        cand_fids{i} = resize_fid(img_s, resize_s, t_fid);
    end    

    % Resize img to 300x300
    img = imresize(img, [300 300]);
    img_s = size(img);

    % Put it all in data
    data.img = img;
    data.n_cand = n_cand;
    data.img_s = img_s;
    data.cand_fids = cand_fids;
end

function [fid] = resize_fid(img_s, resize_s, fid)
    
    %
    ratio_1 = resize_s(1) / img_s(1);
    ratio_2 = resize_s(2) / img_s(2);

    %
    fid(:, 1) = fid(:, 1) * ratio_1;
    fid(:, 2) = fid(:, 2) * ratio_2;
    fid = uint16(fid);

    %
    fid(find(fid<1)) = 1;
    fid(find(fid>resize_s(1))) = resize_s(1);

end


