function [data] = feature_extraction(data)

    %
    run('/home/mallikarjun/softwares/vlfeat-0.9.20/toolbox/vl_setup');
    
    % For each method, get the feature representation 
    data.cand_feat = cell(data.n_cand, 1);
    for i=1:data.n_cand
        data.cand_feat{i} = get_feat(data.img, data.cand_fids{i});
    end
       
end
