function [feat] = get_feat(img, fids)

    if(size(img,3)==3)
        img = single(rgb2gray(img));
    else
        img = single(img);
    end
    n_fids = size(fids, 1);
    feat = cell(n_fids, 1);
    for i=1:n_fids
        x = fids(i, 2);
        y = fids(i, 1);
        hog_feat = get_hog_feature(x, y, img);
        sift_feat = get_sift_feature(x, y, img);
        feat{i} = [hog_feat single(sift_feat)];
    end
end

function hog_feat = get_hog_feature(x, y, im)
    im_size = size(im);
    x1 = uint32(max(x - (im_size(1,2)*0.015),1)); 
    x2 = uint32(min(x + (im_size(1,2)*0.015), im_size(1,2))); 
    y1 = uint32(max(y - (im_size(1,1)*0.015),1)); 
    y2 = uint32(min(y + (im_size(1,1)*0.015), im_size(1,1))); 
   
    if( (x1>=x2) || (y1>=y2) )
        x1 = im_size(1,2);
        x2 = x1;
        y1 = im_size(1,1);
        y2 = y1;
    end
    im_part = im(y1:y2, x1:x2);
    im_part = imresize(im_part, [10 10]);
        
    hog_feat = vl_hog(im_part, 3);
    hog_feat = reshape(hog_feat, [1,size(hog_feat(:))]);
end

function sift_feat = get_sift_feature(x, y, im)
    scales = [5 8];
    sift_feat = [];
    for j=1:2
        fc = double([x; y; scales(j); 0]);
        [~, sift_feat_t] = vl_sift(im,'frames',fc);
        sift_feat = [sift_feat sift_feat_t'];
    end
end

