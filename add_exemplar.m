function [data] = add_exemplar(data)

    % Load the already existing exemplars 
    load('../data/exemplars.mat');
    
    %
    data.n_exem = n_exem;
    data.exem_feat = exem_feat;
    data.exem_fid = exem_fid;

end
