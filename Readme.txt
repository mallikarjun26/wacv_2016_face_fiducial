Usage:

main.m 

Input
Takes in two types of arguments.
  1. Argument 1: Image
  2. Argument 2 to n: Fiducial detection output of candidate algorithms of 20 fiducials. 

Output
Provides two set of output.
  1. Output by kNN
  2. Output by Opt.

Please go through the code. Most of it is self explanatory.
