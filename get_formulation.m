function [data] = get_formulation(data)

    f = get_f(data);

    [A, b] = get_A_and_b(data);

    [Aeq, Beq] = get_Aeq_and_Beq(data);

    unary_length = length(data.unary);
    pairwise_length = length(data.pairwise);

    lb = zeros(unary_length+pairwise_length, 1);
    ub = ones(unary_length+pairwise_length, 1);

    formulation.f = f;
    formulation.A = A;
    formulation.b = b;
    formulation.Aeq = Aeq;
    formulation.Beq = Beq;
    formulation.lb = lb;
    formulation.ub = ub;

    data.formulation = formulation;

end

function f = get_f(data)

    f = zeros( length(data.unary) + length(data.pairwise), 1 );
    
    f(1:length(data.unary)) = data.unary;
    f(1+length(data.unary):(length(data.unary) + length(data.pairwise))) = data.pairwise;

end

function [A, b] = get_A_and_b(data)

    %
    nodes = length(data.unary);
    sum_unary_pairwise = length(data.unary) + length(data.pairwise);
    A = zeros(3*length(data.pairwise), sum_unary_pairwise);
    b = zeros(3*(length(data.pairwise)),1);

    %
    count = 1;
    for pix1=1:nodes
        for pix2=pix1:nodes

            method1 = ceil(pix1/20);
            method2 = ceil(pix2/20);
            part1 = mod(pix1-1,20) + 1;
            part2 = mod(pix2-1,20) + 1;

            if(part1 ~= part2)
                r1 = (count-1)*3 + 1;
                r2 = r1 + 1;
                r3 = r2 + 1;

                A(r1, pix1) = 1;
                A(r1, pix2) = 1;
                A(r1, nodes+count) = -1; 
                b(r1) = 1;
                
                A(r2, pix1)  = -1;
                A(r2, nodes+count) = 1;
                b(r2) = 0;

                A(r3, pix2)  = -1;
                A(r3, nodes+count) = 1;
                b(r3) = 0;

                count = count + 1;
            end

        end
    end

end

function [Aeq, Beq] = get_Aeq_and_Beq(data)

    %
    unary_length = length(data.unary);
    pairwise_length = length(data.pairwise);

    %
    Aeq = zeros(20, unary_length+pairwise_length);  % withing part.
    Beq = ones(20, 1);

    %
    for i=1:20
        for j=1:data.n_cand
            Aeq(i, i + ((j-1)*20)) = 1;
        end
    end

end
