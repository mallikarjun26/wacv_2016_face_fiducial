function [data] = get_unary(data)

    unary = Inf * ones(1, 20*data.n_cand);
    unary_exem_map = zeros(1, 20*data.n_cand);
    
    for i=1:data.n_cand
        for j=1:20
            for k=1:data.n_exem 
                cand_part_feat = data.cand_feat{i}{j};
                exem_part_feat = data.exem_feat{k}{j};
                temp_score = pdist([cand_part_feat; exem_part_feat]);

                index = (i-1)*20 + j;

                if(unary(index) > temp_score)
                    unary(index) = temp_score;
                    unary_exem_map(index) = k;
                end
            end
        end
    end
    
    unary = ( unary - min(unary) ) / range(unary);

    data.unary = unary;
    data.unary_exem_map = unary_exem_map;

end
