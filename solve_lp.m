function data = solve_lp(data)    

    addpath('/home/mallikarjun/softwares/mosek/7/toolbox/r2013a');

    f = data.formulation.f;
    A = data.formulation.A;
    b = data.formulation.b;
    Aeq = data.formulation.Aeq;
    Beq = data.formulation.Beq;
    lb = data.formulation.lb;
    ub = data.formulation.ub;

    tic;
    disp('Running LP ..');
    data.x = linprog(f, A, b, Aeq, Beq, lb, ub);
    disp(['Time taken to converge:' num2str(toc)]);

end
