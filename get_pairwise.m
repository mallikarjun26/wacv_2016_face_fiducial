function data = get_pairwise(data)

    pairwise = [];

    for i=1:length(data.unary)
        for j=i:length(data.unary)
            m1 = ceil(i/20);
            m2 = ceil(j/20);

            p1 = mod(i-1,20) + 1;
            p2 = mod(j-1,20) + 1;

            if(p1 ~= p2)
                e1_fid = data.exem_fid{data.unary_exem_map(i)};
                e2_fid = data.exem_fid{data.unary_exem_map(j)};
                pose_diff = get_pose_diff(e1_fid, e2_fid);

                pairwise = [pairwise; pose_diff];
            end
        end
    end

    pairwise = ( pairwise - min(pairwise) ) / range(pairwise);
    
    data.pairwise = pairwise;
end

function pose_diff = get_pose_diff(pose1, pose2)

    %%
    pose1(:,1) = pose1(:,1) - mean(pose1(:,1));
    pose1(:,2) = pose1(:,2) - mean(pose1(:,2));
    pose2(:,1) = pose2(:,1) - mean(pose2(:,1));
    pose2(:,2) = pose2(:,2) - mean(pose2(:,2));

    s = sum( pose1(:,1).*pose2(:,1) + pose1(:,2).*pose2(:,2) ) ./ sum( pose1(:,1).^2 + pose1(:,2).^2 );

    pose1(:,1) = pose1(:,1) * s;
    pose1(:,2) = pose1(:,2) * s;

    pose1 = pose1(:)';
    pose2 = pose2(:)';

    pose_diff = pdist(double([pose1; pose2]));

end
