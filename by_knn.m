function [o_knn] = by_knn(data)

    %
    dist_matrix = Inf * ones(data.n_cand, data.n_exem);

    % Iterate over the candidate algorithms 
    for i=1:data.n_cand
        % Iterate over the exemplars
        for j=1:data.n_exem 
            dist_matrix(i,j) = app_dist(data.cand_feat{i}, data.exem_feat{j});
        end
    end
            
    % Select based on least score
    [value, index] = min(dist_matrix(:));
    [cand, exem] = ind2sub(size(dist_matrix), index);

    % Select the corresponding candidate fiducials as ouput by kNN
    o_knn = data.cand_fids{cand};

end

function [dist] = app_dist(cand_feat, exem_feat) 
    dist = 0;
    %
    for i=1:size(cand_feat, 1);
        dist = dist + pdist([cand_feat{i}; exem_feat{i}]);
    end
end
