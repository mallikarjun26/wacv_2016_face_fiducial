function [o_opt, o_knn] = main(img, varargin) 

    % Validate and rescale
    %   data.img    - Input Image
    %   data.n_cand  - Number of candidate algorithms 
    %   data.img_s  - Input image size
    %   data.cand_fids - Fiducials of candidate algorithms. Cell of size n_cand. 
    data = validate(img, varargin);

    % Feature representation
    %   data.cand_feat - Candidate algorithms features. Cell of size n_cand.
    data = feature_extraction(data);

    % Attach exemplar data as well
    %   data.n_exem    - Number of exemplars
    %   data.exem_feat - Exemplars features. Cell of size n_exem
    data = add_exemplar(data);

    % Method 1 output
    %   o_opt - Fiducials selected "by Optimization"
    o_opt = by_opt(data);

    % Method 2 output
    %   o_knn - Fiducials selected "by kNN"
    o_knn = by_knn(data);

end
